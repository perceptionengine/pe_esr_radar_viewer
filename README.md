# pe_esr_radar_viewer

Node to visualize AS Delphi RadarTracks:

## Subscriptions

|Topic|Type|
|---|---|
|`NAMESPACE/as_tx/radar_tracks`|`radar_msgs/RadarTrackArray`|

## Publications

|Topic|Type|
|---|---|
|`NAMESPACE/track_markers`|`visualization_msgs/MarkerArray`|

Markers Workspaces:
``

## How to build
Existing workspace:

1. Clone this repo
`cd workspace/src && git clone git@gitlab.com:perceptionengine/pe_esr_radar_viewer.git`
1. Compile
`cd .. && catkin build`
1. Source
`source devel/setup.bash`
 
In a new workspace:
1. `mkdir -p pe_esr_ws/src`
1. `cd pe_esr_ws/src`
1. `git clone git@gitlab.com:perceptionengine/pe_esr_radar_viewer.git`
1. `cd ..`
1. `catkin build`
1. `source devel/setup.bash`

### How to launch
`rosrun pe_esr_viewer pe_esr_viewer.py`

### For ROSA setup:
`roslaunch pe_esr_viewer pe_esr_viewer.launch`

![alt text](doc/rosa.png "Logo Title Text 1")
