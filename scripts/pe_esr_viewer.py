#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('pe_esr_radar_viewer')

import rospy
import math
import tf
import visualization_msgs.msg
import geometry_msgs.msg
import std_msgs.msg
import radar_msgs.msg
import automotive_platform_msgs.msg
import csv
from os.path import expanduser
from datetime import datetime

class PeEsrRadarViewer:
    def __init__(self):
        tracks_topic = "as_tx/radar_tracks"
        speed_topic = "vehicle/speed"
        new_speed_topic = "/new/vehicle/speed"
        rospy.Subscriber(speed_topic, automotive_platform_msgs.msg.Speed, self.ego_speed_callback)
        rospy.Subscriber(new_speed_topic, automotive_platform_msgs.msg.Speed, self.ego_speed_callback)
        rospy.Subscriber(tracks_topic, radar_msgs.msg.RadarTrackArray, self.radar_tracks_callback)
        self.markers_pub_ = rospy.Publisher('track_markers', visualization_msgs.msg.MarkerArray, queue_size=1)
        self.marker_life_ = rospy.Duration(0.1)
        self.sequence_ = 0

        frame_id = rospy.get_param('~frame_id', 'radar')
        self.save_file_ = rospy.get_param('~save_file', False)

        self.red_ = std_msgs.msg.ColorRGBA(1, 0, 0, 1)
        self.green_ = std_msgs.msg.ColorRGBA(0, 1, 0, 1)
        self.yellow_ = std_msgs.msg.ColorRGBA(1, 1, 0, 1)

        self.radar_tracks_speed_ = {}
        self.ego_speed_ = 0
        self.speed_thr_between_frames_ = 5

        now = datetime.now()

        if (self.save_file_):
            csv_file_path_ = expanduser("~") + '/radar_tracks_' + frame_id + "_" + now.strftime("%d%m%Y_%H%M%S") + '.csv'
            self.csv_file_ptr_ = open(csv_file_path_, 'w')
            self.csv_writer = csv.writer(self.csv_file_ptr_)
            self.csv_writer.writerow(["sequence", "timestamp", "frame", "id", "x", "y", "speed", "vel_x", "vel_y", "acc_x", "acc_y"])
            rospy.loginfo("[%s] Saving CSV file to %s", rospy.get_name(), csv_file_path_)

        rospy.loginfo("[%s] Listening to %s for RadarTrackArray", rospy.get_name(), tracks_topic)

    def ego_speed_callback(self, speed_msg):
        self.ego_speed_ = speed_msg.speed
        #print("Ego Speed: {:.2f} m/s".format(speed_msg.speed))

    def is_almost_equal(self, val_a, val_b, threshold=0.1):
        if math.fabs(val_a - val_b) < threshold:
            return True
        else:
            return False

    def radar_tracks_callback(self, radar_tracks_msg):
        cur_header = radar_tracks_msg.header
        vis_markers = visualization_msgs.msg.MarkerArray()
        vis_markers.markers = []
        for track in radar_tracks_msg.tracks:
            cur_track = track#radar_msgs.msg.RadarTrack()

            if len(cur_track.track_shape.points) > 0:
                # box_marker = self.create_box_marker(cur_track.)
                pt = cur_track.track_shape.points[0]
                speed = math.sqrt(
                                  cur_track.linear_velocity.x*cur_track.linear_velocity.x +
                                  cur_track.linear_velocity.y*cur_track.linear_velocity.y
                                  )
                angle = math.atan2(cur_track.linear_velocity.y,
                                   cur_track.linear_velocity.x)

                arrow_color = self.yellow_
                if cur_track.linear_velocity.x < 0:
                    arrow_color = self.red_
                if abs(cur_track.linear_velocity.x) < 0.01 and abs(cur_track.linear_velocity.y) > 0:
                    arrow_color = self.green_
                text_marker = self.create_text_marker(pt.x, pt.y, pt.z,
                                                      cur_track.track_id,
                                                      speed,
                                                      cur_header,
                                                      self.marker_life_)
                arrow_marker = self.create_arrow_marker(pt.x, pt.y, pt.z,
                                                        angle,
                                                        cur_header,
                                                        cur_track.track_id,
                                                        self.marker_life_,
                                                        arrow_color,
                                                        speed)
                if (self.ego_speed_ >= 2.0 and not self.is_almost_equal(speed, self.ego_speed_, speed/5)) \
                    or (self.ego_speed_ < 2.0 and not self.is_almost_equal(speed, self.ego_speed_, 0.5)) \
                    and cur_track.track_id in self.radar_tracks_speed_ \
                    and math.fabs(self.radar_tracks_speed_[cur_track.track_id] -  speed) < self.speed_thr_between_frames_:
                    # vis_markers.markers.append(box_marker)
                    vis_markers.markers.append(arrow_marker)

                    vis_markers.markers.append(text_marker)

                # store current id and speed
                self.radar_tracks_speed_[cur_track.track_id] = speed

                if (self.save_file_):
                    self.csv_writer.writerow([self.sequence_,
                                              "{:010d}.{:09d}".format(cur_header.stamp.secs, cur_header.stamp.nsecs),
                                              cur_header.frame_id,
                                              cur_track.track_id,
                                              pt.x, pt.y,
                                              speed,
                                              cur_track.linear_velocity.x,
                                              cur_track.linear_velocity.y,
                                              cur_track.linear_acceleration.x,
                                              cur_track.linear_acceleration.y])

        self.markers_pub_.publish(vis_markers)
        self.sequence_ = self.sequence_ + 1

    def create_arrow_marker(self, pos_x, pos_y, pos_z, angle, header, marker_id, duration, color, speed = 1.0):
        q = tf.transformations.quaternion_from_euler(0.0, 0.0, angle)
        box_marker = visualization_msgs.msg.Marker()
        box_marker.ns = 'track_arrow'
        box_marker.id = marker_id
        box_marker.type = visualization_msgs.msg.Marker.ARROW
        box_marker.action = visualization_msgs.msg.Marker.ADD
        box_marker.lifetime = duration
        box_marker.pose.position.x = pos_x
        box_marker.pose.position.y = pos_y
        box_marker.pose.position.z = pos_z + 3.0
        box_marker.scale.x = 1.0
        box_marker.scale.y = 0.5
        box_marker.scale.z = 0.5
        box_marker.header = header
        box_marker.pose.orientation = geometry_msgs.msg.Quaternion(*q)
        box_marker.color = color

        return box_marker

    def create_text_marker(self, pos_x, pos_y, pos_z, track_id, label, header, duration):
        text_marker = visualization_msgs.msg.Marker()
        text_marker.ns = 'track_id'
        text_marker.id = track_id
        text_marker.type = visualization_msgs.msg.Marker.TEXT_VIEW_FACING
        text_marker.action = visualization_msgs.msg.Marker.ADD
        text_marker.lifetime = duration
        text_marker.pose.position.x = pos_x
        text_marker.pose.position.y = pos_y
        text_marker.pose.position.z = pos_z + 5.0
        text_marker.scale.z = 1.0
        text_marker.header = header
        text_marker.color = std_msgs.msg.ColorRGBA(1, 1, 1, 1)
        text_marker.text = "{:.1f}m/s <{}>".format(label, track_id)

        return text_marker

    def create_polygon_marker(self, points, header, marker_id, color, duration):
        box_marker = visualization_msgs.msg.Marker()
        box_marker.ns = 'track_polygon'
        box_marker.id = marker_id
        box_marker.type = visualization_msgs.msg.Marker.LINE_LIST
        box_marker.action = visualization_msgs.msg.Marker.ADD
        box_marker.lifetime = duration
        box_marker.points = points
        box_marker.scale.x = 0.2
        box_marker.header = header
        box_marker.color = color #std_msgs.msg.ColorRGBA(0, 0.4, 0.8, 0.9)

        return box_marker

if __name__ == '__main__':
    rospy.init_node('pe_esr_radarviewer', anonymous=True)

    node = PeEsrRadarViewer()
    rospy.spin()
